import express from 'express';

import * as LinksController from '../controllers/links-controller';
import checkToken from '../middlewares/checkToken';
import isAdmin from '../middlewares/isAdmin';

const router = express.Router();

router.post('/links', checkToken, isAdmin, LinksController.create);
router.put('/links', checkToken, isAdmin, LinksController.update);
router.get('/links', LinksController.getAll);
router.get('/links/:url', LinksController.getByUrl);
router.delete('/links/:url', LinksController.deleteLink);

export default router;