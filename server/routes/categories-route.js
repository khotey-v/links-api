import express from 'express';

import * as CategoriesController from '../controllers/categories-controller';
import checkToken from '../middlewares/checkToken';
import isAdmin from '../middlewares/isAdmin';

const router = express.Router();

router.post('/categories', checkToken, isAdmin, CategoriesController.create);
router.put('/categories', checkToken, isAdmin, CategoriesController.update);
router.get('/categories', CategoriesController.getAll);
router.get('/categories/:url', CategoriesController.getByUrl);
router.get('/categories/:url/items', CategoriesController.getItemsByCategoryUrl);
router.get('/categories/:url/links', CategoriesController.getLinksByItemUrl);
router.delete('/categories/:url', CategoriesController.deleteCategory)

export default router;