import express from 'express';

import * as UserController from '../controllers/user-controller';
import checkToken from '../middlewares/checkToken';

const router = express.Router();

router.get('/user-info', checkToken, UserController.getUserInfo);

export default router;
