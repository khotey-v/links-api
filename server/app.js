import express from 'express';
import compression from 'compression';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import { isDev } from './config';
import cors from 'cors'

const app = express();

if (isDev) {
  app.use(morgan('tiny'));
}

app.use(cors());
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true  }));

export default app;
