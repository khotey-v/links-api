const ENV = process.env.NODE_ENV || 'dev'

export const isProd = ENV === 'prod'
export const isDev = ENV === 'dev'

export default ENV
