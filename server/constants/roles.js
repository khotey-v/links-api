const USER_ROLE = 'USER';
const ADMIN_ROLE = 'ADMIN';

export {
  USER_ROLE,
  ADMIN_ROLE,
}

export default {
  admin: ADMIN_ROLE,
  user: USER_ROLE
};