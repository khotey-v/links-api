import jwt from 'jsonwebtoken';

import config from '../config';

export default function (req, res, next) {
  const token = req.get('XSRF-TOKEN');
  let tokenObj;

  if (!token) {
    return next({
      status: 401,
      message: 'Unauthorized. Token is not exist.'
    });
  }

  try {
    tokenObj = jwt.verify(token, config.secret);
  } catch ({ message }) {
    return next({
      status: 403,
      message
    });
  }

  req.token = tokenObj;
  next();
}
