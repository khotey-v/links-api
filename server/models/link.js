import mongoose, { Schema } from 'mongoose';

const LinkSchema = new Schema({
  name: { type: String, unique: true, lowercase: true, required: true },
  url: { type: String, unique: true, lowercase: true, required: true },
  category: { type: String, ref: 'Category', required: true }
});

export default mongoose.model('Link', LinkSchema);