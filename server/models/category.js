import mongoose, { Schema } from 'mongoose';

const CategorySchema = new Schema({
  name: { type: String, unique: true, lowercase: true, required: true },
  url: { type: String, unique: true, lowercase: true, required: true },
  parent: { type: String, ref: 'Category' }
});

export default mongoose.model('Category', CategorySchema);