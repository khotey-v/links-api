import User from '../models/user';

export const getUserInfo = async (req, res, next) => {
  const { token: { _id } } = req;
  let user;

  try {
    user = await User.findOne({ _id }, '-password');
  } catch({ message }) {
    return next({
      status: 500,
      message
    });
  }

  res.json(user);
}
