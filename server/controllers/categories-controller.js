import Category from '../models/category';
import Link from '../models/link';

export const create = async (req, res, next) => {
  const data = req.body;
  let category;

  try {
    category = await Category.create(data);
  } catch ({ message }) {
    return next({
      status: 400,
      message
    })
  }

  res.json(category);
};

export const update = async (req, res, next) => {
  const data = req.body;
  let category;

  try {
    category = await Category.findOneAndUpdate({ _id: data._id }, { $set: data }, { 'new': true });
  } catch ({ message }) {
    return next({
      status: 400,
      message
    })
  }

  res.json(category);
};

export const getAll = async (req, res, next) => {
  let categories;
  let query = {};

  if (req.query.parent === 'no') {
    query.parent = null;
  }

  if (req.query.parent === 'yes') {
    query.parent = { $ne: null };
  }

  try {
    categories = await Category
      .find(query)
      .populate('parent');
  } catch ({ message }) {
    return next({
      status: 400,
      message
    })
  }

  res.json(categories);
};

export const getByUrl = async (req, res, next) => {
  const { url } = req.params;
  let category;

  try {
    category = await Category.findOne({ url });
  } catch ({ message }) {
    return next({
      status: 400,
      message
    })
  }

  if (!category) {
    return next({
      status: 404,
      message: `Category with url "${url} not found."`
    })
  }

  res.json(category);
};

export const getItemsByCategoryUrl = async (req, res, next) => {
  const { url } = req.params;
  let category;
  let items;

  try {
    category = await Category.findOne({ url });
  } catch ({ message }) {
    return next({
      status: 400,
      message
    })
  }

  if (!category) {
    return next({
      status: 404,
      message: `Category with url "${url}" not found.`
    })
  }

  try {
    items = await Category.find({ parent: category._id });
  } catch ({ message }) {
    return next({
      status: 400,
      message
    })
  }

  res.json(items);
};

export const getLinksByItemUrl = async (req, res, next) => {
  const { url } = req.params;
  let item;
  let links;

  try {
    item = await Category.findOne({ url });
  } catch ({ message }) {
    return next({
      status: 400,
      message
    })
  }

  if (!item) {
    return next({
      status: 404,
      message: `Item with url "${url}" not found.`
    })
  }

  try {
    links = await Link.find({ category: item._id });
  } catch ({ message }) {
    return next({
      status: 400,
      message
    })
  }

  res.json(links);
};

export const deleteCategory = async (req, res, next) => {
  const { url } = req.params;
  let category;

  try {
    category = await Category.findOne({ url });
  } catch ({ message }) {
    return next({
      status: 400,
      message
    })
  }

  if (!category) {
    return next({
      status: 404,
      message: `Category with url "${url} not found."`
    })
  }

  category.remove();

  res.json(category);
}