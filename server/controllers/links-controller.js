import Link from '../models/link';

export const create = async (req, res, next) => {
  const data = req.body;
  let link;

  try {
    link = await Link.create(data);
  } catch ({ message }) {
    return next({
      status: 400,
      message
    })
  }

  res.json(link);
};

export const update = async (req, res, next) => {
  const data = req.body;
  let link;

  try {
    link = await Link.findOneAndUpdate({ _id: data._id }, { $set: data }, { 'new': true });
  } catch ({ message }) {
    return next({
      status: 400,
      message
    })
  }

  res.json(link);
};

export const getAll = async (req, res, next) => {
  let links;

  try {
    links = await Link.find({}).populate('category');
  } catch ({ message }) {
    return next({
      status: 400,
      message
    })
  }

  res.json(links);
};

export const getByUrl = async (req, res, next) => {
  const { url } = req.params;
  let link;

  try {
    link = await Link.findOne({ url });
  } catch ({ message }) {
    return next({
      status: 400,
      message
    })
  }

  if (!link) {
    return next({
      status: 404,
      message: `Link with url "${url}" not found.`
    })
  }

  res.json(link);
};

export const deleteLink = async (req, res, next) => {
  const { url } = req.params;
  let link;

  try {
    link = await Link.findOne({ url });
  } catch ({ message }) {
    return next({
      status: 400,
      message
    })
  }

  if (!link) {
    return next({
      status: 404,
      message: `Link with url "${url}" not found.`
    })
  }

  link.remove();

  res.json(link);
};