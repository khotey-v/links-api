import app from './app';
import errorHandler from './middlewares/errorHandler';
import authRoute from './routes/auth-route';
import categoriesRoute from './routes/categories-route';
import linksRoute from './routes/links-route';
import userRoute from './routes/user-route';

// TODO: add data field which contain fetched data
// TODO: add verisons to API
// TODO: store tokens in redis and delete after definitive time
app.use('/api', authRoute);
app.use('/api', categoriesRoute);
app.use('/api', linksRoute);
app.use('/api', userRoute);
app.use('*', (req, res, next) => {
  next({
    status: 404,
    message: 'Not Found.'
  })
});
app.use(errorHandler());

export default app;
